package njs.tag;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A map that is efficient in its {@link #get(Object)} method but not anything else. Works only on {@link Taggable}
 * objects in which it stores membership information. Therefore {@link #get(Object)} is fast for {@link Taggable}
 * objects that are in a small number of maps (or sets) at the same time. This is particularly useful when you have a
 * small number of large maps (or sets). This map implementation is backed by a {@link TagMap}. The iteration order is
 * insertion order.
 *
 * <b>IMPORTANT:</b> This map works with <i>object identity</i>, not {@link Object#equals(Object)} or
 * {@link Object#hashCode()}.
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author Jeff Smits
 * @see Collection
 * @see Map
 * @see Taggable
 * @see TagSet
 */
public class TagMap<K extends Taggable, V> extends AbstractMap<K, V> {
    private static final AtomicInteger uidCounter = new AtomicInteger(0);
    final List<K> elements;
    private final int uid = TagMap.uidCounter.getAndAdd(2);
    private final int indexUID = uid + 1;
    private int removed = 0;

    /**
     * Constructs a new, empty map.
     */
    public TagMap() {
        elements = new ArrayList<>();
    }

    /**
     * Constructs a new, empty map with the specified initial capacity.
     *
     * @param initialCapacity the initial capacity of the map
     * @throws IllegalArgumentException if the initial capacity is less than zero
     */
    public TagMap(int initialCapacity) {
        elements = new ArrayList<>(initialCapacity);
    }

    /**
     * Returns the number of elements in this set (its cardinality).
     *
     * @return the number of elements in this set (its cardinality)
     */
    @Override
    public int size() {
        return elements.size() - removed;
    }

    /**
     * Returns <tt>true</tt> if this map contains a mapping for the specified key. (There can be at most one such
     * mapping.)
     *
     * @param key key whose presence in this map is to be tested
     * @return <tt>true</tt> if this map contains a mapping for the specified key
     * @throws ClassCastException   if the key is not an instance of {@link Taggable}
     * @throws NullPointerException if the specified key is null
     */
    @Override
    public boolean containsKey(Object key) {
        return ((Taggable) key).tagStore().get(indexUID) != null;
    }

    /**
     * Returns the value to which the specified key is mapped, or <tt>null</tt> if this map contains no mapping for the
     * key. (There can be at most one such mapping.)
     * <p>
     * A return value of <tt>null</tt> does not necessarily indicate that the map contains no mapping for the
     * <tt>key</tt>; it's also possible that the map explicitly maps the <tt>key</tt> to <tt>null</tt>. The
     * {@link #containsKey(Object)} operation may be used to distinguish these two cases.
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or <tt>null</tt> if this map contains no mapping for the
     * key
     * @throws ClassCastException   if the key is not an instance of {@link Taggable}
     * @throws NullPointerException if the specified key is null
     */
    @Override
    public V get(Object key) {
        return ((Taggable) key).tagStore().get(uid);
    }

    /**
     * Associates the specified value with the specified key in this map. If the map previously contained a mapping for
     * the key, the old value is replaced by the specified value. (A map <tt>m</tt> is said to contain a mapping for a
     * key <tt>k</tt> if and only if <tt>m.containsKey(k)</tt> would return true.)
     *
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with <tt>key</tt>, or <tt>null</tt> if there was no mapping for
     * <tt>key</tt>. (A <tt>null</tt> return can also indicate that the map previously associated <tt>null</tt> with
     * <tt>key</tt>)
     * @throws ClassCastException   if the key is not an instance of {@link Taggable}
     * @throws NullPointerException if the specified key is null
     */
    @Override
    public V put(K key, V value) {
        TagStore tagStore = key.tagStore();
        V result = tagStore.put(uid, value);
        if (result == null && tagStore.get(indexUID) == null) {
            tagStore.put(indexUID, elements.size());
            elements.add(key);
        }
        return result;
    }

    /**
     * Removes the mapping for a key from this map if it is present. (The map can contain at most one such mapping.)
     * <p>
     * Returns the value to which this map previously associated the key, or <tt>null</tt> if the map contained no
     * mapping for the key.
     * <p>
     * A return value of <tt>null</tt> does not <i>necessarily</i> indicate that the map contained no mapping for the
     * key; it's also possible that the map explicitly mapped the key to <tt>null</tt>.
     * <p>
     * The map will not contain a mapping for the specified key once the call returns.
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with <tt>key</tt>, or <tt>null</tt> if there was no mapping for
     * <tt>key</tt>.
     * @throws ClassCastException   if the key is not an instance of {@link Taggable}
     * @throws NullPointerException if the specified key is null
     */
    @Override
    public V remove(Object key) {
        TagStore tagStore = ((Taggable) key).tagStore();
        V result = tagStore.remove(uid);
        int index = tagStore.remove(indexUID);
        elements.set(index, null);
        removed++;
        return result;
    }

    /**
     * Removes all of the elements from this collection. The collection will be empty after this method returns.
     */
    @Override
    public void clear() {
        for (K key : elements) {
            if (key != null) {
                TagStore tagStore = key.tagStore();
                tagStore.remove(uid);
                tagStore.remove(indexUID);
            }
        }
        elements.clear();
        removed = 0;
    }

    /**
     * Returns a {@link Set} view of the mappings contained in this map. The set is backed by the map, so changes to the
     * map are reflected in the set, and vice-versa. If the map is modified while an iteration over the set is in
     * progress (except through the iterator's own remove operation, or through the setValue operation on a map entry
     * returned by the iterator) the results of the iteration are undefined. The set supports element removal, which
     * removes the corresponding mapping from the map, via the {@link Iterator#remove()}, {@link Set#remove(Object)},
     * {@link Set#removeAll(Collection)}, {@link Set#retainAll(Collection)} and {@link Set#clear()} operations. It does
     * not support the {@link Set#add(Object)} or {@link Set#addAll(Collection)} operations.
     *
     * @return a set view of the mappings contained in this map
     */
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return new EntrySet();
    }

    @Override
    public void finalize() {
        clear();
    }

    private final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator(TagMap.this.elements.iterator(), TagMap.this.size());
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            //noinspection unchecked
            return TagMap.this.containsKey(((Entry) o).getKey());
        }

        @Override
        public boolean remove(Object o) {
            return TagMap.this.remove(o) != null;
        }

        @Override
        public int size() {
            return TagMap.this.size();
        }

        @Override
        public void clear() {
            TagMap.this.clear();
        }
    }

    private final class EntryIterator implements Iterator<Map.Entry<K, V>> {
        private final Iterator<K> elementsIterator;
        private int seen = 0;
        private int total;

        EntryIterator(Iterator<K> elementsIterator, int total) {
            this.elementsIterator = elementsIterator;
            this.total = total;
        }

        @Override
        public boolean hasNext() {
            return seen < total;
        }

        @Override
        public Entry next() {
            K next = null;
            while (next == null) {
                next = elementsIterator.next();
            }
            seen++;
            return new Entry(next, TagMap.this.get(next));
        }

        @Override
        public void remove() {

        }
    }

    private class Entry implements Map.Entry<K, V> {
        private final K key;
        private V value;

        Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(Object o) {
            //noinspection unchecked
            value = (V) o;
            return TagMap.this.put(key, value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            @SuppressWarnings("unchecked") Entry entry = (Entry) o;
            return key == entry.key && Objects.equals(value, entry.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }
    }
}
