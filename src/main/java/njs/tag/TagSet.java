package njs.tag;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * A set that is efficient in its {@link #contains(Object)} method but not anything else. Works only on {@link Taggable}
 * objects in which it stores membership information. Therefore {@link #contains(Object)} is fast for {@link Taggable}
 * objects that are in a small number of sets (or maps) at the same time. This is particularly useful when you have a
 * small number of large sets (or maps). This set implementation is backed by a {@link TagMap}. The iteration order is
 * insertion order.
 *
 * <b>IMPORTANT:</b> This set works with <i>object identity</i>, not {@link Object#equals(Object)} or
 * {@link Object#hashCode()}.
 *
 * @param <E> the type of elements maintained by this set
 * @author Jeff Smits
 * @see Collection
 * @see Set
 * @see Taggable
 * @see TagMap
 */
public class TagSet<E extends Taggable> extends AbstractSet<E> {
    private TagMap<E, Object> map;

    /**
     * Constructs a new, empty set.
     */
    public TagSet() {
        this(0);
    }

    /**
     * Constructs a new set containing the elements in the specified collection.
     *
     * @param c the collection whose elements are to be placed into this set
     * @throws NullPointerException if the specified collection is null
     */
    public TagSet(Collection<? extends E> c) {
        this(c.size());
        addAll(c);
    }

    /**
     * Constructs a new, empty set; the backing {@link TagMap} instance has
     * the specified initial capacity.
     *
     * @param initialCapacity the initial capacity of the backing map
     * @throws IllegalArgumentException if the initial capacity is less than zero
     */
    public TagSet(int initialCapacity) {
        map = new TagMap<>(initialCapacity);
    }

    /**
     * Returns an iterator over the elements in this set. The elements are returned in order of insertion.
     *
     * @return an Iterator over the elements in this set
     */
    @Override
    public Iterator<E> iterator() {
        return new TagSetIterator<>(map.elements.iterator(), map.size());
    }

    /**
     * Returns the number of elements in this set (its cardinality).
     *
     * @return the number of elements in this set (its cardinality)
     */
    @Override
    public int size() {
        return map.size();
    }

    /**
     * Returns <tt>true</tt> if this set contains the specified element <i>exactly</i>, by object identity (not
     * equality).
     *
     * @param o element whose presence in this set is to be tested
     * @return <tt>true</tt> if this set contains the specified element
     */
    @Override
    public boolean contains(Object o) {
        //noinspection SuspiciousMethodCalls
        return map.containsKey(o);
    }

    /**
     * Ensures that this collection contains the specified element (optional operation). Returns <tt>true</tt> if this
     * collection changed as a result of the call. (Returns <tt>false</tt> if this collection does not permit duplicates
     * and already contains the specified element.)
     *
     * @param e element whose presence in this collection is to be ensured
     * @return <tt>true</tt> if this collection changed as a result of the call
     */
    @Override
    public boolean add(E e) {
        boolean result = !contains(e);
        map.put(e, null);
        return result;
    }

    /**
     * Removes a single instance of the specified element from this collection, if it is present (optional operation).
     * Returns <tt>true</tt> if this collection contained the specified element (or equivalently, if this collection
     * changed as a result of the call).
     *
     * @param o element to be removed from this collection, if present
     * @return <tt>true</tt> if an element was removed as a result of this call
     */
    @Override
    public boolean remove(Object o) {
        boolean result = contains(o);
        map.remove(o);
        return result;
    }

    /**
     * Removes all of the elements from this collection. The collection will be empty after this method returns.
     */
    @Override
    public void clear() {
        map.clear();
    }

    private static final class TagSetIterator<E extends Taggable> implements Iterator<E> {
        private final Iterator<E> elementsIterator;
        private int seen = 0;
        private int total;

        TagSetIterator(Iterator<E> elementsIterator, int total) {
            this.elementsIterator = elementsIterator;
            this.total = total;
        }

        @Override
        public boolean hasNext() {
            return seen < total;
        }

        @Override
        public E next() {
            E next = null;
            while (next == null) {
                next = elementsIterator.next();
            }
            seen++;
            return next;
        }
    }
}
