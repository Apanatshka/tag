package njs.tag;

/**
 * <tt>Taggable</tt> classes can store parts of {@link TagSet} and {@link TagMap} in their objects through a
 * {@link TagStore}. This makes these sets and maps so fast. A proper implementation of this interface will have a
 * (non-static) field of type {@link TagStore}, initialized with {@link TagStore#create()}, and the {@link #tagStore()}
 * method is just a getter for that field.
 */
public interface Taggable {
    /**
     * @return the tagstore of this object
     */
    TagStore tagStore();
}
