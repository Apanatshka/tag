package njs.tag;

import java.util.HashMap;
import java.util.Map;

/**
 * The <tt>TagStore</tt> maps unique IDs of {@link TagMap}s to the value that a {@link Taggable} key maps to. Each
 * {@link Taggable} class has a <tt>TagStore</tt> to handle this. Only the {@link #create()} method is exposed for
 * creating a new <tt>TagStore</tt>.
 */
@SuppressWarnings("unchecked")
public class TagStore {
    private Map<Integer, Object> map = new HashMap<>();

    private TagStore() {
    }

    /**
     * TagStore constructor.
     *
     * @return a new TagStore
     */
    public static TagStore create() {
        return new TagStore();
    }

    <V> V get(int tag) {
        return (V) map.get(tag);
    }

    <V> V put(int tag, V value) {
        return (V) map.put(tag, value);
    }

    <V> V remove(int uid) {
        return (V) map.remove(uid);
    }
}
