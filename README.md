# TagSet

Java implementation of Set / Map with very fast contains/lookup vs. slow other operations, using a tagging approach (dynamic object fields). Notably these Sets and Maps use object identity instead of equality/hash. 



